function string4(obj={}) {
    let name = "";
    if(obj.first_name) name+=obj.first_name.slice(0,1).toUpperCase() + obj.first_name.slice(1).toLowerCase()
    if(obj.middle_name) name += " " + obj.middle_name.slice(0,1).toUpperCase() + obj.middle_name.slice(1).toLowerCase()
    if(obj.last_name) name += " " + obj.last_name.slice(0,1).toUpperCase() + obj.last_name.slice(1).toLowerCase()
    return name;
}

module.exports = string4