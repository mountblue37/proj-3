function string1(str) {
    if(typeof str !== 'string') return 0;
    const s = str.replace("$","");
    const num = Number.parseFloat(s);
    return isNaN(num) ? 0 : num
}

module.exports = string1;
