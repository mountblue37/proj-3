function string2(str) {
    //unicode of 0-9 : 48 - 57
    for(i = 0; i < str.length; i++) {
        if(str.charAt(i) == ".") continue;
        if(!(str.charCodeAt(i) >= 48 && str.charCodeAt(i) <=57)) return [];
    }
    return str.split(".")
   
}

module.exports = string2;
